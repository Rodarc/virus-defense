extends Node

onready var spawner = get_node("Gameplay/Spawner")
onready var organo = get_node("Gameplay/Organo")
onready var hud = get_node("HUD")
export (PackedScene) var Globulo

export var player_cooldown = 4.0
var current_cooldown = 0.0

func _ready():
    randomize()
    hud.show_prepare_message()
    hud.connect("create", self, "create_globulo")
    get_node("Timer").start(3)
    pass

func _physics_process(delta):
    if current_cooldown:
        current_cooldown = clamp(current_cooldown - delta, 0.0, player_cooldown)
    else:
        HabilitarBoton()

func _on_Timer_timeout():
    StartWave()
    pass # Replace with function body.

func StartWave():
    hud.hide_prepare_message()
    spawner.start_next_wave()


func _on_Spawner_wave_ended():
    if spawner.wave_left():
        get_node("Timer").start(3)
    else:
        if hud.has_method("show_win_message"):
            hud.show_win_message()
        pass
        #mostrar el mensaje de ganaste
        #quiza pausar la ejecucion del juego


func _on_Organo_infected():
    #he perdido el juego, mostrar el mensaje de perdiste
    if hud.has_method("show_game_over_message"):
        hud.show_game_over_message()
    pass # Replace with function body.

func create_globulo(spawn_position):
    var scene = get_tree().get_current_scene()
    var globulo_instance = Globulo.instance()
    scene.add_child(globulo_instance)
    globulo_instance.position = spawn_position
    current_cooldown = player_cooldown
    DeshabilitarBoton()

func HabilitarBoton():
    hud.habilitar_boton()

func DeshabilitarBoton():
    hud.deshabilitar_boton()
