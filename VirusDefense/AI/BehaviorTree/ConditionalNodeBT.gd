extends NodeBT

class_name ConditionalNodeBT

var Condition: FuncRef = null # condiciones referentes al actor, no al arbol

func _ready():
    pass


func execute(delta):
    if(Condition.call_func(delta)): # para funciones del tipo Is...
        status = StatusBT.Completed
    else:
        status = StatusBT.Failed
    #print("[ConditionalBT] ", NameStatusBT[status])
    return status
