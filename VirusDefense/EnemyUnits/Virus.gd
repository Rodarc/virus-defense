extends "res://EnemyUnits/Antigeno.gd"

enum States {Idle, Walk, Attack, Damage, Dying, Cloning, Chase}
var current_state = States.Idle

var walk_time = 2.0

var attack_power = 25.0

onready var IABT = get_node("BehaviorTree")
var BTStatus_attack = NodeBT.StatusBT.Ready # tener este estado, esta mal, buscar alguna alternativa
onready var burbujas = get_node("Visuals/Burbujas")
onready var hit = get_node("Visuals/Hit")

func _ready():
    ConstructBehaviourTree()
    change_state(States.Walk)
    pass

func _physics_process(delta):
    IABT.execute(delta)
    match current_state:
        States.Idle:
            current_attack_cooldown = clamp(current_attack_cooldown - delta, 0, attack_cooldown)
            pass
        States.Walk:
            # me muevo hacia una direccion
            # move_direction = DestinationDirection
            movement = move_direction * movement_max_speed
            move_and_collide(movement * delta)
            # si colisiono deberia cambiar de direccion, pero esto lo deberia hacer la IA creo, si no fuera asi, agregaria esta parte aqui
            pass
        States.Attack:
            pass

func enter_state(state):
    #incia las animaciones correspondietes, etc
    #cambia parametros
    match state: # o current_state
        States.Idle:
            movement = Vector2(0.0, 0.0)
            move_direction = Vector2(0.0, 0.0)
            get_node("Visuals/AnimatedSprite").play("Idle")
        States.Walk:
            set_new_destination()
            burbujas.emitting = true
        States.Attack:
            get_node("Visuals/AnimatedSprite").play("Attack")
            movement = Vector2(0.0, 0.0)
            move_direction = Vector2(0.0, 0.0)
        States.Dying:
            get_node("Visuals/AnimatedSprite").play("Die")
            stop_movement()
            set_collision_layer_bit(1, false)
            set_collision_mask_bit(0, false)
            set_collision_mask_bit(1, false)
            set_collision_mask_bit(2, false)
            # inciar la animacion.
            # no moverme
            # desactivar las colisiones, para que los globlulos lo tengan en cunata y todos lo puedan atravesar


func exit_state(state):
    #puede hacer delays si son necesarios.
    match state:
        States.Idle:
            pass
        States.Attack:
            current_attack_cooldown = attack_cooldown
        States.Walk:
            burbujas.emitting = false

func change_state(new_state):
    exit_state(current_state)
    print("Virus state: ", new_state)
    current_state = new_state # deberia estar dentro de enterstate?
    enter_state(new_state)


func set_new_destination():
    # retonar un punto aleatorio del escenario, 
    # creo que podria ser una direccion de destino mejo
    # debereia reemplazar la direccion de movimiento automaticamente
    # las probabilidades que tanto me muevo en una u otra direccion deberia depender de la posicion en el escenario
    
    var new_direction = Vector2(rand_range(-10, -1), rand_range(-3, 3))
    move_direction = new_direction.normalized()

func attack():
    # inica la animacion de atacar
    # hace dañó al enemigo que este actualmente
    if enemy_target and current_attack_cooldown == 0.0:
        # make_damage(enemy_target)# temporalmente aqui
        change_state(States.Attack)

func make_damage(enemy):
    if enemy and enemy.has_method("take_damage"):# verification for safety
        enemy.call("take_damage", attack_power)

func die():
    change_state(States.Dying)

func BTAvanzar(delta):
    if current_state != States.Walk:
        change_state(States.Walk)
    walk_time -= delta
    if walk_time <= 0.0:
        set_new_destination()
        walk_time = rand_range(0.5, 2.0)
    return NodeBT.StatusBT.Running

func BTIsEnemyInAttackRange(delta):
    var res = enemy_target in get_node("AttackRange").get_overlapping_bodies()
    return res


func BTAttack(delta): # necesito tener un estado para el atque, saber si ya termine y retornar completed en ese momento
    if (current_state != States.Attack): # la comprobacion de energia se debe hacer en el arbol, no aqui, ya que la cantidad de energia determina la estrategia tambien, deberia estar en ambos lados
        attack()
        BTStatus_attack = NodeBT.StatusBT.Running
    return BTStatus_attack


func get_closer_enemy():
    var enemies = get_node("DetectionRange").get_overlapping_bodies()
    var enemy_closer = null
    var distancia_minima = INF # 100000.0
    for enemy in enemies:
        var distancia = (position - enemy.position).length()
        #var distancia = position.distance_to(object.position)
        if distancia < distancia_minima:
            enemy_closer = enemy
            distancia_minima = distancia
    return enemy_closer

func BTIsEnemyInDetectionRange(delta):
    var enemy_closer = get_closer_enemy()
    enemy_target = enemy_closer
    return enemy_target != null

func BTChase(delta):#este deberia recibir parametro hacia que enemigo, sin emabro qquien esta haciedo esteo se el propio detect enemy, internamente, mejorar esta comunicacion
    
    if enemy_target:
        #print("chasing")
        if BTIsEnemyInAttackRange(delta): # si entra en mi rango, de ataque debo dar como completada la tarea, aun que no se si esto sera necesario
            return NodeBT.StatusBT.Completed
        if current_state != States.Walk:
            change_state(States.Walk)
        move_direction = (enemy_target.position - position).normalized()
        return NodeBT.StatusBT.Running
        # deberia retornar Completed, si me he ubicado lo suficientemente cerca del objetivo, quiza hasta que entre en colision con el o algo
    return NodeBT.StatusBT.Failed

func BTIsDying(delta):
    return current_state == States.Dying


func _on_AnimatedSprite_animation_finished():
    ._on_AnimatedSprite_animation_finished()
    var animation_name = get_node("Visuals/AnimatedSprite").get_animation()
    match animation_name:
        "Attack":
            # quiza deberia ver que no haya inciado otra, asumo que para eso es change animation, si he llegaodo a eto, quiere dicri que ni inicie algun ataque, ni recibi ataque, que cortar la animacion de ataque
            change_state(States.Idle)
            current_attack_cooldown = attack_cooldown
            BTStatus_attack = NodeBT.StatusBT.Completed
        "Damaged":
            change_state(States.Idle)
        "Die":
            emit_signal("died", self)
            queue_free()# asumo que esto se hace aqui, o que lo haga el spawn?


func _on_AnimatedSprite_frame_changed():
    # espero esta funcion no sea pesada
    ._on_AnimatedSprite_frame_changed()
    var AnimSprite = get_node("Visuals/AnimatedSprite")
    var animation_name = AnimSprite.get_animation()
    match animation_name:
        "Attack":
            if AnimSprite.get_frame() == 2:
                # el frame 2 de la animacion de ataque es donde deberia ejecutarse el daño
                make_damage(enemy_target)

func take_damage(damage: float):
    .take_damage(damage)
    hit.visible = true
    get_node("Timer").start(0.5)

        



func ConstructBehaviourTree():
    if IABT:
        IABT.root = SelectorNodeBT.new()
        
        # Rama para el comportamiento de morir, en relaidad cuando mueres no haces anda
        var node = ConditionalNodeBT.new()
        node.Condition = funcref(self, "BTIsDying")
        IABT.root.add_child(node)
        
        # Rama para el comportamiento de ataque
        node = SequenceNodeBT.new()
        node.add_child(ConditionalNodeBT.new())
        node.add_child(ActionNodeBT.new())
        node.childs[0].Condition = funcref(self, "BTIsEnemyInAttackRange")
        node.childs[1].Action = funcref(self, "BTAttack")
        IABT.root.add_child(node)
        
        # Rama para chase
        node = SequenceNodeBT.new()
        node.add_child(ConditionalNodeBT.new())
        node.add_child(ActionNodeBT.new())
        node.childs[0].Condition = funcref(self, "BTIsEnemyInDetectionRange")
        node.childs[1].Action = funcref(self, "BTChase")
        IABT.root.add_child(node)
        
        # Rama avanzar
        node = SequenceNodeBT.new()
        node.add_child(ActionNodeBT.new())
        node.childs[0].Action = funcref(self, "BTAvanzar")
        IABT.root.add_child(node)



func _on_Timer_timeout():
    hit.visible = false
