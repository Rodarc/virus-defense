extends KinematicBody2D

export var health_max: float = 100.0
var health: float = health_max

signal died

onready var orientation: int = scale.x

var move_direction: Vector2 = Vector2() #direccion a al a que me movere, variable para ser alterada por los inputs y la gravedad
export var movement_max_speed: float = 150.0

var movement: Vector2 = Vector2()

export var attack_cooldown = 1.5
var current_attack_cooldown = 0.0

var enemy_target = null
var enemy_target_is_in_range = false

func _on_DetectionRange_body_entered(body):
    pass # Replace with function body.


func _on_DetectionRange_body_exited(body):
    pass # Replace with function body.


func take_damage(damage: float):
    #cuando puedo recibir dañó, y cuando no?
    #por erro podria tener una doble validacion?
    #si recibio ataque mientras me estan atacando, debo iniciar una nueva animacon de ataque, pueod esto?
    health = clamp(health - damage, 0, health_max)
    #emit_signal("health_changed", health)
    print(health)
    if !health:
        # TODO Died mejor llamar a la funcion die
        # esta funcion cambiara el estado a died para que haga animaciones de muerto, y luego cuando temrine recien llamar queue free
        die()
        # queue_free()
        # change_state(States.Died)
    else:
        # ejecucion de efectos visuales de que ha recibido dañó, no habra una animacion de recibir daño, podra seguir atacando si le atacan.
        pass
        # change_state(States.Damage)
        # esto no hara que se ejecute nuevamente la animacion, o si?, probar
        # en caso de que no lo haga, aqui debo iniciar nuevamente las animacion
    
    #inciar la animaion de recibir dañó
    #actualizar la vida
    #se regrsa al estado idle, despues de terminar
    #estas animaciones estan funcionando com timers a la ver, tener cuidado con esto
    #ver la correcta secuencia de eventos para que todo sea comodo y pueda recivbir daño
    pass


func _on_AttackRange_body_entered(body):
    pass # Replace with function body.


func _on_AttackRange_body_exited(body):
    pass # Replace with function body.


func _on_AnimatedSprite_animation_finished():
    pass # Replace with function body.


func _on_AnimatedSprite_frame_changed():
    pass # Replace with function body.

func stop_movement():
    movement = Vector2()
    move_direction = Vector2()

func die():
    #quiza esto no este aqui, ya que podria llamarse cuando la animacion de morir termine
    #emit_signal("died", self)
    pass
