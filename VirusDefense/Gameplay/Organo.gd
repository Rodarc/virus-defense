extends Area2D

# Cuandos virus puede recibir antes de infectarse, tambien podria ser daño recibido si cambio la mecanica y hago que reciba daño
export (int, 0, 10) var max_health = 10 # quiza necesite un setter, para que ponga el valor de health donde le toca
export (int, 0, 10) var health

signal health_changed
signal infected

func _ready():
    health = max_health
    pass


func _on_Organo_body_entered(body):
    # en teoria solo detecto enemigos
    # los contare por ahora, y cuando llegue al limite, dire que estoy infectado
    
    health = clamp(health - 1, 0, max_health)
    emit_signal("health_changed", health)
    if !health:
        emit_signal("infected")
