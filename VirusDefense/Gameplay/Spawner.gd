extends Area2D

export (int, 0, 10) var numero_enemigos_oleada_1 = 10
export (int, 0, 20) var numero_enemigos_oleada_2 = 20
export (int, 0, 40) var numero_enemigos_oleada_3 = 40

export (float, 1, 4) var max_cooldown_spawn_oleada_1 = 3
export (float, 1, 3) var max_cooldown_spawn_oleada_2 = 2
export (float, 1, 2) var max_cooldown_spawn_oleada_3 = 1

export (float, 0, 3) var min_cooldown_spawn_oleada_1 = 0.25
export (float, 0, 2) var min_cooldown_spawn_oleada_2 = 0.25
export (float, 0, 1) var min_cooldown_spawn_oleada_3 = 0.25

enum Waves {NoWave, Wave1, Wave2, Wave3}
var current_wave = Waves.NoWave setget set_current_wave, get_current_wave

var spawn: bool = false setget set_spawn, get_spawn
var spawned_antigenos: Array = []
# spawnea mientras esto este verdadero, cuando se acaban los enemigos se pone en falso

export (PackedScene) var Antigeno
# cada antigeno deberia tener su probabilidad


signal wave_ended
signal wave_started

# cuando mueren todos los spawneados, luego de un delay esto se vuelve verdadero, e inicia la siguiente oleada
# quiza esta parte la tenga que controlar el nivel o alguien mas, en lugar de que lo haga solo

var current_cooldown = 0
var antigens_left = 0


#el nivel llamara a ser current wave, establecera la oleada que se debe ejecutar
#luego cuando lo requiera llamara a set spawn true
#cuando la oleada termine y emita la señal, spawn estar en false
#el nivel enteonces, esperara lo que tenga que eseprar, establecera el current wave y llamara a set spawn true para empezar
func _ready():
    # descomentar para probar sin el gamemode
    # set_current_wave(Waves.Wave1)
    # set_spawn(true)
    pass

func _physics_process(delta):
    if spawn && antigens_left:
        match current_wave:
            Waves.NoWave:
                pass
            Waves.Wave1:
                waving(delta, min_cooldown_spawn_oleada_1, max_cooldown_spawn_oleada_1)
            Waves.Wave2:
                waving(delta, min_cooldown_spawn_oleada_2, max_cooldown_spawn_oleada_2)
            Waves.Wave3:
                waving(delta, min_cooldown_spawn_oleada_3, max_cooldown_spawn_oleada_3)

func waving(delta, min_cooldown, max_cooldown):
    current_cooldown -= delta
    if current_cooldown <= 0.0:
        spawn_antigen(Antigeno, get_random_position())
        current_cooldown = rand_range(min_cooldown, max_cooldown)
        antigens_left -= 1
        if antigens_left <= 0:
            spawn = false
            #creo que no necesito esto

func set_spawn(new_value):
    spawn = new_value
    if spawn:
        emit_signal("wave_started", current_wave)
        #cuando se emita esta señal, se mostrara en la interfaz la wave en la que se esta, necesitaran hacerlo con un get_current_wave

func get_spawn():
    return spawn

func set_current_wave(new_value):
    current_wave = new_value
    match current_wave:
        Waves.NoWave:
            pass
        Waves.Wave1:
            antigens_left = numero_enemigos_oleada_1
            current_cooldown = rand_range(min_cooldown_spawn_oleada_1, max_cooldown_spawn_oleada_1)
        Waves.Wave2:
            antigens_left = numero_enemigos_oleada_2
            current_cooldown = rand_range(min_cooldown_spawn_oleada_2, max_cooldown_spawn_oleada_2)
        Waves.Wave3:
            antigens_left = numero_enemigos_oleada_3
            current_cooldown = rand_range(min_cooldown_spawn_oleada_3, max_cooldown_spawn_oleada_3)

func get_current_wave():
    return current_wave

func spawn_antigen(Antigen, spawn_position): #spawn position must be global
    var scene = get_tree().get_current_scene()
    var antigeno_instance = Antigen.instance()
    scene.add_child(antigeno_instance)
    antigeno_instance.connect("died", self, "on_antigen_killed")
    antigeno_instance.position = spawn_position
    spawned_antigenos.append(antigeno_instance)
    #deberia conectarme a su señal died, para saber cuando a muerto y quitarlo del array para saber cuando termino la oleada.
    # y esta clase emitir la señal de que la oleada temrino al que controle el juego, quiza el nivel

func get_random_position():
    var area = get_node("CollisionShape2D").shape
    var size = area.extents
    var x = rand_range(-size.x, size.x)
    var y = rand_range(-size.y, size.y)
    return Vector2(x, y) + position

func start_next_wave():
    if (wave_left()):
        set_current_wave(current_wave + 1)
        set_spawn(true)

func wave_left():
    #retorna el numero de oleadas faltantes
    return Waves.Wave3 - current_wave

func on_antigen_killed(antigen):
    spawned_antigenos.erase(antigen)
    if spawned_antigenos.empty():
        emit_signal("wave_ended")
