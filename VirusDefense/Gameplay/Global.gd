extends Node

export (String, FILE, "*.tscn") var home_menu = "res://UI/HomeMenu.tscn"

func _ready():
    pass

func restart_level():
    get_tree().reload_current_scene()

func go_to_home_menu():
    get_tree().change_scene(home_menu)
