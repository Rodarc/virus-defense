extends CanvasLayer

onready var win_message = get_node("WinMessage")
onready var game_over_message = get_node("GameOverMessage")
onready var prepare_message = get_node("MessageWidget/PrepararseLabel")
onready var pause_menu = get_node("PauseMenu")
onready var health_bar = get_node("HealthWidget/HealthBar")
onready var wave_bar = get_node("WaveWidget/WaveBar")
onready var wave_label = get_node("WaveWidget/WaveLabel")
onready var proxy = get_node("Proxy")
onready var create_button = get_node("HBoxContainer/Create")

var infeccion_text = "Infección {current_wave} / {num_waves}"

var pressing: bool = false
var target_position: Vector2 = Vector2()

signal create

func _ready():
    var organo = get_node("../Gameplay/Organo")
    var max_health = organo.max_health
    health_bar.max_value = max_health
    update_health_ui(max_health)
    organo.connect("health_changed", self, "update_health_ui")
    
    var spawner = get_node("../Gameplay/Spawner")
    var num_waves = spawner.Waves.size() - 1
    wave_bar.max_value = num_waves
    update_wave_ui(0)
    spawner.connect("wave_started", self, "update_wave_ui")

func _input(event):
    #if pressing and event is InputEventScreenTouch and event.pressed:
        #pressing = true
        #target_position = event.position
        #proxy.set_position(target_position - proxy.rect_size/2)
        #proxy.visible = true
        #print("pressing")
        #print(target_position)
    if pressing and event is InputEventScreenDrag: #era elif
        target_position = event.position
        
        proxy.set_position(target_position - proxy.rect_size/2)
    #elif pressing and event is InputEventScreenTouch and !event.pressed:
        #print("release")
        #print(target_position)
        #proxy.visible = false
        #Create()
        
        #pass


func Create():
    emit_signal("create", target_position)


func show_win_message():
    win_message.visible = true

    
func hide_win_message():
    win_message.visible = false


func show_game_over_message():
    game_over_message.visible = true

    
func hide_game_over_message():
    game_over_message.visible = false


func show_prepare_message():
    prepare_message.visible = true


func hide_prepare_message():
    prepare_message.visible = false


func _on_RestartButton_pressed():
    get_tree().paused = false
    Global.restart_level()


func _on_QuitButton_pressed():
    get_tree().paused = false
    Global.go_to_home_menu()


func _on_NextLevelButton_pressed():
    # no se quien lo procesara, pero por ahora solo reiniciare el nivel ya que solo tengo un nivel
    Global.restart_level()


func _on_PauseButton_pressed():
    get_tree().paused = true
    pause_menu.visible = true


func _on_CloseBUtton_pressed():
    get_tree().paused = false
    pause_menu.visible = false

func update_health_ui(new_health):
    health_bar.value = new_health
    pass

func update_wave_ui(new_value):
    wave_bar.value = new_value
    infeccion_text.format({"current_wave": new_value, "num_waves": wave_bar.max_value})
    wave_label.text = infeccion_text.format({"current_wave": new_value, "num_waves": wave_bar.max_value})
    pass


func _on_Create_button_down():
    pressing = true
        
    target_position = Vector2(-50, -50)
    proxy.set_position(target_position - proxy.rect_size/2)
    proxy.visible = true

    print("press")

    pass # Replace with function body.


func _on_Create_button_up():
    print("resleas")
    proxy.visible = false
    Create()
    target_position = Vector2(-50, -50)
    proxy.set_position(target_position - proxy.rect_size/2)
    
    pass # Replace with function body.

func habilitar_boton():
    create_button.disabled = false

func deshabilitar_boton():
    create_button.disabled = true
