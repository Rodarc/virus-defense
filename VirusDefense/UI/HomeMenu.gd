extends MarginContainer

export (String, FILE, "*.tscn") var first_level

func _ready():
    pass


func _on_NewGameButton_pressed():
    get_tree().change_scene(first_level)
    #repecto a change_scene_to
    #usar packedscenes es para digamos partidas guardades, cuya escena tengo. reuqiero que un resourceloader la levante y la genere, se puede usar para las escenas normales
    #pero es innecesario para la scena por defecto creo
    pass # Replace with function body.


func _on_OptionsButton_pressed():
    pass # Replace with function body.


func _on_QuitButton_pressed():
    get_tree().quit()
    pass # Replace with function body.
